var closeMenuInterval;

$( document ).ready( function(){
	
	//alert($(window).width() + "x" + $(window).height());

	$('body').css('height', $(window).height());

	$('#touch-btn').draggable({
		containment: 'parent',
		revert: true,
		revertDuration: 250,
		scroll: false,
		drag:function(){
				var pos = $('#touch-btn').position();
				if(pos.left < 80 && pos.left > 58 && pos.top < 120) {
				$('#cancel').addClass('open1');
				}
				
				else if( pos.left < 57 && pos.top < 90 ) {
				$('#cancel').addClass('open1').addClass('drop-hover');
				}
		
				else if( pos.left > 110 && pos.left < 135 && pos.top < 120){
				$('#confirm').addClass('open1');
				}
				
				else if( pos.left > 136 && pos.top < 120 ) {
				$('#confirm').addClass('open1').addClass('drop-hover');
				}
		
				else {
				$('#cancel').removeClass('open1').removeClass('drop-hover');
				$('#confirm').removeClass('open1').removeClass('drop-hover');
				}
		},
		
		stop:function(){
					$('#cancel').removeClass('open1').removeClass('drop-hover');
					$('#confirm').removeClass('open1').removeClass('drop-hover');
				}
	});
 
	$('#touch-btn').on('touchstart', function () {
	
		$(this).addClass('active');
		$('#confirm').addClass('open');
		$('#cancel').addClass('open');
		$('.invoice').addClass('animateinvoice');
		$('.user-account').addClass('animate-user-account');
		clearInterval(closeMenuInterval);
		$('.cross').removeClass('fadeout');
		$('.check').removeClass('fadeout');
		
			
	});

	$('#touch-btn').on('touchend', function () {
		$(this).removeClass('active');
		closeMenuInterval = setInterval( function() { closeMenu (); }, 3000);
	});

	$('.droppable').droppable({
		accept: '#touch-btn',
		hoverClass: 'drop-hover',
		tolerance: 'intersect',
		drop:function(){
		
				$('#cancel').removeClass('open1').removeClass('open');
				$('#confirm').removeClass('open1').removeClass('open');
			}
	});

	// Handle User clicks
	$('#cancel').on('touchstart', function () {
		$(this).addClass('drop-hover')
	});

	$('#cancel').on('touchend', function () {
		$(this).removeClass('drop-hover')
	});

	$('#confirm').on('touchstart', function () {
		$(this).addClass('drop-hover')
	});

	$('#confirm').on('touchend', function () {
		$(this).removeClass('drop-hover')
	});

	// Triggered when an accepted draggable is dropped on the droppable
	// or when user clicks #cancel
	$('#cancel').on('drop click', function ( event, ui ) {
		//alert("User canceled.");
		$('#cancel').removeClass('open1').removeClass('open');
		$('.cross').addClass('fadeout');
		$("#touch-btn").draggable({disabled: true});
		$('#touch-btn').off('touchstart');
		$('#touch-btn').addClass('disable');
		closeMenu();
		
	});

	// Triggered when an accepted draggable is dropped on the droppable
	// or when user clicks #confirm
	$('#confirm').on('drop click', function ( event, ui ) {
		//alert("User confirmed.");
		$('#confirm').removeClass('open1').removeClass('open');
		$('.check').addClass('fadeout');
		$("#touch-btn").draggable({disabled: true});
		$('#touch-btn').off('touchstart');
		$('#touch-btn').addClass('disable');
		closeMenu();
	});
	
});



function closeMenu () {
	clearInterval(closeMenuInterval);
	$('#confirm').removeClass('open').removeClass('open1');
	$('#cancel').removeClass('open').removeClass('open1');
	$('.user-account').removeClass('animate-user-account');
	$('.invoice').removeClass('animateinvoice');
	
}

